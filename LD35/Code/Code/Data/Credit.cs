﻿using System;
using UnityEngine;

namespace HD {
    [Serializable]
    public struct Credit {
        public string name;
        [Multiline]
        public string whatTheyDid;
    }
}