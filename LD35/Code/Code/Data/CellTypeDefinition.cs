﻿using System;
using UnityEngine;

namespace HD {
    [Serializable]
    public class CellTypeDefinition {
        public GameObject gameObject;
        public int[] levelRates;
    }
}