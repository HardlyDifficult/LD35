﻿using System;
using UnityEngine;

namespace HD {
    public class GridCell {
        public readonly bool supportsTraps;
        public readonly int x, y;
        public bool canSpawnBadGuy = true;
        public Tile tile;
        public CellType type;
        internal bool hasBadGuy;
        internal bool isEndGame;
        public GridCell(int x, int y, CellType type, bool supportsTraps = true) {
            this.x = x;
            this.y = y;
            this.type = type;
            this.supportsTraps = supportsTraps && type != CellType.StoneLava;
        }

        public void Show() {
            if(tile == null) {
                var gameObject = GameObject.Instantiate(Grid.instance.cellTypes[(int)type].gameObject,
                    CellManager.offset +
                    new Vector3(x, 0, y),
                    Quaternion.identity) as GameObject;
                var transform = gameObject.transform;
                tile = gameObject.GetComponent<Tile>();
                tile.cell = this;

                if((canSpawnBadGuy || hasBadGuy) && type != CellType.Wall && (hasBadGuy || UnityEngine.Random.Range(0, 20 - CellManager.currentLevel) == 0)) {
                    GameObject.Instantiate(Grid.instance.badGuy,
                        transform.position + Grid.instance.badGuy.transform.position, Grid.instance.badGuy.transform.localRotation);
                }
                if(isEndGame) {
                    GameObject.Instantiate(Grid.instance.nextLevelItem, Grid.instance.nextLevelItem.transform.position
                        + transform.position, Grid.instance.nextLevelItem.transform.rotation);
                }
            }
        }
    }
}