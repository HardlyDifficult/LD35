﻿using System;

namespace HD {
    public enum CellType {
        Normal, SomethingElse, Grass, Tree,
        TrapDoor, BrokenTrapDoor, Sand,
        // Special
        StoneLava,
        Water, Wall
    }
}