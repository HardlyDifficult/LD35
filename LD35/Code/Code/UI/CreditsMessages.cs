﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace HD {
    public class CreditsMessages : MonoBehaviour {

        #region Constant data
        public float extraSpeedPerMagnitude = .01f;
        public Credit[] messages;
        public float pauseTime = 1;
        public float speed = 1;
        Vector2 exitPositionRange;
        RectTransform myTransform;
        #endregion Constant data

        #region Running Data
        float pauseTimeRemaining;
        Vector2 targetPosition;
        Text textName, textDescription;
        float totalMagnitudeTillExit;
        #endregion Running Data

        int lastSelected = -1;
        public void Awake() {
            myTransform = GetComponent<RectTransform>();
            exitPositionRange = new Vector2((Screen.width + myTransform.rect.width) / 2,
                (Screen.height + myTransform.rect.height) / 2);
            var textComponents = GetComponentsInChildren<Text>();
            textName = textComponents[0];
            textDescription = textComponents[1];
            Reset();
        }

        public void Update() {
            bool complete;
            myTransform.localRotation = myTransform.localRotation.MoveTowardsOnZOnly(Quaternion.identity,
                speed * Time.deltaTime / 5,
                out complete, 0, 360);
            if(pauseTimeRemaining > 0) {
                pauseTimeRemaining -= Time.deltaTime;
            } else {
                myTransform.anchoredPosition = myTransform.anchoredPosition.MoveTowards(targetPosition,
                    speed * Time.deltaTime, out complete, extraSpeedPerMagnitude, totalMagnitudeTillExit);
                if(complete) {
                    if(totalMagnitudeTillExit == 0) {
                        pauseTimeRemaining = pauseTime;
                        SelectExitPosition();
                    } else {
                        Reset();
                    }
                }
            }
        }

        void Reset() {
            SelectRandomMessage();
            targetPosition = Vector2.zero;
            totalMagnitudeTillExit = 0;
            myTransform.localRotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360));
        }

        void SelectExitPosition() {
            switch(UnityEngine.Random.Range(0, 3)) {
            case 0:
                targetPosition = new Vector2(
                   0,
                   UnityEngine.Random.Range(0, 2) == 0 ? exitPositionRange.y : -exitPositionRange.y);
                break;
            case 1:
                targetPosition = new Vector2(
                   UnityEngine.Random.Range(0, 2) == 0 ? exitPositionRange.x : -exitPositionRange.x,
                   0);
                break;
            default:
            case 2:
                targetPosition = new Vector2(
                   UnityEngine.Random.Range(0, 2) == 0 ? exitPositionRange.x : -exitPositionRange.x,
                   UnityEngine.Random.Range(0, 2) == 0 ? exitPositionRange.y : -exitPositionRange.y);
                break;
            }
            totalMagnitudeTillExit = targetPosition.magnitude;
        }

        void SelectRandomMessage() {
            int i;
            do {
                i = UnityEngine.Random.Range(0, messages.Length);
            } while(i == lastSelected);
            lastSelected = i;
            textName.text = messages[i].name;
            textDescription.text = messages[i].whatTheyDid;
        }
    }
}