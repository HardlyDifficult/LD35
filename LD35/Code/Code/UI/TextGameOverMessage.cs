﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace HD {
    public class TextGameOverMessage : MonoBehaviour {
        Text text;

        public void Awake() {
            text = GetComponent<Text>();
            var time = DateTime.Now - GameObject.Find("Time").GetComponent<TextTime>().startTime;
            text.text = $"You made it to level {CellManager.currentLevel + 1} in {(int)time.TotalMinutes} mins {time.Seconds} secs.";
        }
    }
}