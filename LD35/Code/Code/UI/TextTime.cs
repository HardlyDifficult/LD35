﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace HD {
    public class TextTime : MonoBehaviour {
        public DateTime startTime;
        Text text;
        public void Awake() {
            text = GetComponent<Text>();
            startTime = DateTime.Now;
        }

        public void Update() {
            var timePassed = DateTime.Now - startTime;
            text.text = $"Time: {(int)timePassed.TotalMinutes}:{timePassed.Seconds.ToString("D2")}:{timePassed.Milliseconds.ToString("D3")}";
        }
    }
}