﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace HD {
    public class FadeInAndOut : MonoBehaviour {

        #region Const data
        public float timeTillComplete = 1;
        public float timeToPause = 1;
        Text[] text;
        #endregion Const data

        #region Run data
        float fadeTimePassed;
        bool isFadingIn;
        float pauseTimeRemaining;
        #endregion Run data

        public void Awake() {
            text = GetComponentsInChildren<Text>();
            pauseTimeRemaining = timeToPause;
        }

        public void Update() {
            if(pauseTimeRemaining > 0) {
                pauseTimeRemaining -= Time.deltaTime;
            } else {
                fadeTimePassed += Time.deltaTime;
                float alpha;
                if(fadeTimePassed > timeTillComplete) {
                    alpha = isFadingIn ? 1 : 0;
                    if(isFadingIn) {
                        pauseTimeRemaining = timeToPause;
                    }
                    isFadingIn = !isFadingIn;
                    fadeTimePassed = 0;
                } else {
                    alpha = fadeTimePassed / timeTillComplete;
                    if(!isFadingIn) {
                        alpha = 1 - alpha;
                    }
                }
                for(int i = 0; i < text.Length; i++) {
                    text[i].color = new Color(text[i].color.r, text[i].color.b, text[i].color.g, alpha);
                }
            }
        }
    }
}