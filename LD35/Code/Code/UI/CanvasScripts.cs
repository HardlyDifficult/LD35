﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasScripts : MonoBehaviour {
    public GameObject gameOverScreen;
    public int onTapOffExistingButtonsLoadScene = -1;
    EventSystem eventSystem;
    public event Action onTapOffExistingButtons;
    public void SceneChange(int sceneId) {
        SceneManager.LoadScene(sceneId);
    }

    void Start() {
        eventSystem = EventSystem.current;
    }

    void Update() {
        if((onTapOffExistingButtons != null || onTapOffExistingButtonsLoadScene >= 0)
            && Input.GetMouseButtonDown(0) && !eventSystem.IsPointerOverGameObject()) {
            if(onTapOffExistingButtons != null) {
                onTapOffExistingButtons();
            } else {
                SceneChange(onTapOffExistingButtonsLoadScene);
            }
        }
    }
}