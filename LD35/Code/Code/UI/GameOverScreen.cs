﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour {
    public CanvasGroup inGameUIGroup;
    CanvasScripts canvasScripts;

    public void OnDisable() {
        try {
            canvasScripts.onTapOffExistingButtonsLoadScene = -1;
            inGameUIGroup.blocksRaycasts = true;
            inGameUIGroup.interactable = true;
        } catch { }
    }

    public void OnEnable() {
        canvasScripts.onTapOffExistingButtonsLoadScene = 0;
        inGameUIGroup.blocksRaycasts = false;
        inGameUIGroup.interactable = false;
    }

    void Awake() {
        canvasScripts = GameObject.Find("Canvas").GetComponent<CanvasScripts>();
    }
}