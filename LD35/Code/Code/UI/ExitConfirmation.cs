﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ExitConfirmation : MonoBehaviour {
    public CanvasGroup inGameUIGroup;
    CanvasScripts canvasScripts;

    public void OnDisable() {
        canvasScripts.onTapOffExistingButtons -= Cancel;
        try {
            inGameUIGroup.blocksRaycasts = true;
            inGameUIGroup.interactable = true;
        } catch { }
    }

    public void OnEnable() {
        canvasScripts.onTapOffExistingButtons += Cancel;
        inGameUIGroup.blocksRaycasts = false;
        inGameUIGroup.interactable = false;
    }

    void Awake() {
        canvasScripts = GameObject.Find("Canvas").GetComponent<CanvasScripts>();
    }

    void Cancel() {
        gameObject.SetActive(false);
    }
}