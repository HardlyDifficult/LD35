﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace HD {
    public class TextLevel : MonoBehaviour {
        Text text;
        public void Awake() {
            text = GetComponent<Text>();
        }

        public void Update() {
            text.text = $"Level {CellManager.currentLevel + 1} / 10";
        }
    }
}