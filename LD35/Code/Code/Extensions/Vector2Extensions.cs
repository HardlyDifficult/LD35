﻿using System;
using UnityEngine;

namespace HD {
    public static class Vector2Extensions {
        public static Vector2 MoveTowards(this Vector2 from, Vector2 to, float speed, out bool complete,
            float addMagnitudeToSpeed, float totalMagnitude) {
            var deltaPosition = to - from;
            var magnitude = deltaPosition.magnitude;
            if(addMagnitudeToSpeed > 0) {
                speed += Math.Abs((totalMagnitude > 0 ? totalMagnitude - magnitude : magnitude) * addMagnitudeToSpeed);
            }
            if(speed >= magnitude) {
                complete = true;
                return to;
            } else {
                complete = false;
                return from + deltaPosition * (speed / magnitude);
            }
        }
    }
}