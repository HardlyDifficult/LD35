﻿using System;

namespace HD {
    public static class FloatExtensions {
        public static float Lerp(this float from, float to, float by) {
            if(from < to) {
                return Math.Min(from + by, to);
            } else {
                return Math.Max(from - by, to);
            }
        }
    }
}