﻿using System;
using UnityEngine;

namespace HD {
    public static class RotationExtensions {
        public static Quaternion MoveTowardsOnZOnly(this Quaternion from, Quaternion to, float speed, out bool complete,
            float addMagnitudeToSpeed, float totalMagnitude) {
            var magnitude = Quaternion.Angle(from, to);
            if(addMagnitudeToSpeed > 0) {
                speed += Math.Abs((totalMagnitude > 0 ? totalMagnitude - magnitude : magnitude) * addMagnitudeToSpeed);
            }

            var toEuler = to.eulerAngles;
            var euler = from.eulerAngles;
            var rotation = Quaternion.Euler(euler.x,
                euler.y,
                //Math.Abs(toEuler.y - euler.y) > speed ?
                //euler.y + (toEuler.y - euler.y) * speed : toEuler.y,
                Math.Abs(toEuler.z - euler.z) > speed ?
                euler.z + (toEuler.z - euler.z) * speed : toEuler.z);
            if(rotation == to) {
                complete = true;
                return to;
            } else {
                complete = false;
                return rotation;
            }
        }
    }
}