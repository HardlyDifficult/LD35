﻿using System;
using UnityEngine;

namespace HD {
    public static class CellManager {
        const int yPerLevel = 20;
        static int __xUnits = 6, __yUnits = 9;
        static GridCell[][,] cellsForAllLevels;
        static int iCurrentLevel, iMaxLevelReached;
        static int[] xOffset, yOffset;

        static CellManager() {
            xOffset = new int[Grid.levelCount];
            yOffset = new int[Grid.levelCount];
        }

        public static int currentLevel {
            get {
                return iCurrentLevel;
            }
        }

        public static Vector3 midPointForCurrentLevel {
            get {
                return new Vector3(xUnits / 2, 0, yUnits / 2) + offset;
            }
        }

        public static Vector3 offset {
            get {
                return new Vector3(xOffset[iCurrentLevel], yPerLevel * iCurrentLevel, yOffset[iCurrentLevel]);
            }
        }

        public static int xUnits {
            get {
                return __xUnits * (iCurrentLevel + 1);
            }
        }

        public static int yUnits {
            get {
                return __yUnits * (iCurrentLevel + 1);
            }
        }
        public static void LoadNextLevel(Vector3 characterOffset) {
            if(iCurrentLevel < 9) {
                iCurrentLevel++;

                xOffset[iCurrentLevel] = (int)Math.Round(characterOffset.x) - xUnits / 2;
                yOffset[iCurrentLevel] = (int)Math.Round(characterOffset.z) - yUnits / 2;

                if(iMaxLevelReached < iCurrentLevel) {
                    iMaxLevelReached = iCurrentLevel;
                    Grid.instance.LoadLevel(iCurrentLevel);
                }
            } else {
                MainCharacter.instance.GameOver(true);
            }
        }

        public static void Reset() {
            iMaxLevelReached = 0;
            cellsForAllLevels = new GridCell[Grid.levelCount][,];
            for(int i = 0; i < Grid.levelCount; i++) {
                iCurrentLevel = i;
                cellsForAllLevels[i] = new GridCell[xUnits, yUnits];
            }

            iCurrentLevel = 0;
            Grid.instance.LoadLevel(0);
        }

        internal static GridCell Create(GridCell gridCell) {
            cellsForAllLevels[iCurrentLevel][gridCell.x, gridCell.y] = gridCell;
            return gridCell;
        }

        internal static GridCell Get(int x, int y, int iLevel = -1) {
            if(iLevel < 0) {
                iLevel = iCurrentLevel;
            }
            if(x >= 0 && x < (__xUnits * (iLevel + 1)) && y >= 0 && y < (__yUnits * (iLevel + 1))) {
                return cellsForAllLevels[iLevel][x, y];
            }

            return null;
        }

        internal static GridCell Get(Vector3 position) {
            var iLevel = (int)Math.Round(position.y / yPerLevel);
            return Get((int)Math.Round(position.x) - xOffset[iLevel],
                (int)Math.Round(position.z) - yOffset[iLevel], iLevel);
        }

        internal static void GetRandomOutskirtPositionInBounds(out int x, out int y, float percentBounds) {
            x = (int)UnityEngine.Random.Range(1, 2 + (xUnits - 3) * percentBounds);
            y = (int)UnityEngine.Random.Range(1, 2 + (yUnits - 3) * percentBounds);
            if(UnityEngine.Random.Range(0, 2) == 0) {
                x = xUnits - x - 1;
            }
            if(UnityEngine.Random.Range(0, 2) == 0) {
                y = yUnits - y - 1;
            }
        }

        internal static void GoDownLevel() {
            if(iCurrentLevel > 0) {
                iCurrentLevel--;
            }
        }

        internal static bool IsInBounds(int x, int y) {
            return x >= 1 && x < xUnits - 1 && y >= 1 && y < yUnits - 1;
        }

        internal static bool IsInBounds(Vector3 position) {
            return (Get(position)?.type ?? CellType.Wall) != CellType.Wall;
        }

        internal static bool IsXOnBoundary(int x) {
            return x == 0 || x == xUnits - 1;
        }

        internal static bool IsYOnBoundary(int y) {
            return y == 0 || y == yUnits - 1;
        }
    }
}