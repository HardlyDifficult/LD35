﻿using System;
using System.Collections;
using UnityEngine;

namespace HD {
    public class Grid : MonoBehaviour {
        public const int levelCount = 10;
        public static Grid instance;
        public CellTypeDefinition[] cellTypes;
        public GameObject character, spikeTrap, badGuy, bridge, nextLevelItem;
        public bool isWorldFlipped;
        public int visibilityRange;
        AudioSource flipSound;
        bool onFlipCooldown;
        public event Action onFlip;
        public void Awake() {
            instance = this;
            CellManager.Reset();
            Instantiate(character, CellManager.midPointForCurrentLevel + character.transform.position, Quaternion.identity);
            flipSound = GameObject.Find("FlipSound").GetComponent<AudioSource>();
        }

        public void LoadLevel(int iLevel) {
            FillSpawnArea(iLevel);
            FillEndGame(iLevel);
            FillGridWithRiver(iLevel);
            FillWithLavaLakes(iLevel);
            FillRemainingGridWithRandomTileTypes(iLevel);
        }

        public void Update() {
            ShowVisibleCells();

            if(!onFlipCooldown && Input.GetButtonDown("Jump")) {
                onFlipCooldown = true;
                isWorldFlipped = !isWorldFlipped;
                flipSound.Play();
                onFlip?.Invoke();
                StartCoroutine(Cooldown());
            }
        }

        void BuildLake(int x, int y, int rate) {
            var xEnd = x + UnityEngine.Random.Range(1, 4 + rate);
            for(; x < xEnd; x++) {
                var yEnd = y + UnityEngine.Random.Range(1, 4 + rate);
                for(; y < yEnd; y++) {
                    if(CellManager.IsInBounds(x, y)) {
                        if(CellManager.Get(x, y) != null) {
                            break;
                        }
                        CellManager.Create(new GridCell(x, y, CellType.StoneLava));
                    }
                }
            }
        }

        private IEnumerator Cooldown() {
            yield return new WaitForSeconds(1);
            onFlipCooldown = false;
        }

        void FillEndGame(int iLevel) {
            int x, y;
            do {
                CellManager.GetRandomOutskirtPositionInBounds(out x, out y, .25f);
            } while(CellManager.Get(x, y) != null);
            CellType type;
            switch(UnityEngine.Random.Range(0, 3)) {
            case 0:
                type = CellType.Normal;
                break;
            case 1:
                type = CellType.Normal;
                //type = CellType.Grass; no no...
                break;
            default:
            case 2:
                type = CellType.SomethingElse;
                break;
            }

            var cell = CellManager.Create(new GridCell(x, y, type, false));
            cell.isEndGame = true;
        }

        void FillGridWithRiver(int iLevel) {
            if(cellTypes[(int)CellType.Water].levelRates[iLevel] > 0) {
                bool isXLocked, lockedCoordMovesPositiveNotNegative;

                int x, y;
                do {
                    CellManager.GetRandomOutskirtPositionInBounds(out x, out y, 0);
                } while(CellManager.Get(x, y) != null);

                if(CellManager.IsXOnBoundary(x)) {
                    isXLocked = true;
                    lockedCoordMovesPositiveNotNegative = x == 0;
                } else {
                    Debug.Assert(CellManager.IsYOnBoundary(y));
                    isXLocked = false;
                    lockedCoordMovesPositiveNotNegative = y == 0;
                }

                do {
                    CellManager.Create(new GridCell(x, y, CellType.Water));
                    int deltaX, deltaY;
                    do {
                        if(UnityEngine.Random.Range(0, 2) == 0) {
                            deltaX = isXLocked ? lockedCoordMovesPositiveNotNegative ? 1 : -1
                                : UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1;
                            deltaY = 0;
                        } else {
                            deltaX = 0;
                            deltaY = !isXLocked ? lockedCoordMovesPositiveNotNegative ? 1 : -1
                                : UnityEngine.Random.Range(0, 2) == 0 ? 1 : -1;
                        }
                    } while(CellManager.Get(x + deltaX, y + deltaY) != null);
                    x += deltaX;
                    y += deltaY;
                } while(!CellManager.IsXOnBoundary(x) && !CellManager.IsYOnBoundary(y));
            }
        }

        void FillRemainingGridWithRandomTileTypes(int iLevel) {
            for(int x = 0; x < CellManager.xUnits; x++) {
                for(int y = 0; y < CellManager.yUnits; y++) {
                    if(CellManager.Get(x, y) == null) {
                        GridCell cell;
                        if(CellManager.IsXOnBoundary(x) || CellManager.IsYOnBoundary(y)) {
                            cell = new GridCell(x, y, CellType.Wall);
                        } else {
                            CellType type = RandomCell(iLevel);

                            cell = new GridCell(x, y, type);
                        }
                        CellManager.Create(cell);
                    }
                }
            }
        }

        void FillSpawnArea(int iLevel) {
            for(int x = -1; x <= 1; x++) {
                for(int y = -1; y <= 1; y++) {
                    var cell = CellManager.Create(new GridCell(CellManager.xUnits / 2 + x, CellManager.yUnits / 2 + y, CellType.Normal, false));
                    cell.canSpawnBadGuy = false;
                }
            }
        }

        void FillWithLavaLakes(int iLevel) {
            if(cellTypes[(int)CellType.StoneLava].levelRates[iLevel] > 0) {
                for(int x = 1; x < CellManager.xUnits - 1; x++) {
                    for(int y = 1; y < CellManager.yUnits - 1; y++) {
                        if(CellManager.Get(x, y) == null && UnityEngine.Random.Range(0, 30) == 0) {
                            BuildLake(x, y, cellTypes[(int)CellType.StoneLava].levelRates[iLevel]);
                        }
                    }
                }
            }
        }

        CellType RandomCell(int iLevel) {
            var count = Enum.GetNames(typeof(CellType)).Length - 3;
            int totalChance = 0;
            for(int i = 0; i < count; i++) {
                totalChance += cellTypes[i].levelRates[iLevel];
            }

            int selected = UnityEngine.Random.Range(0, totalChance + 1);
            for(int i = 0; i < count; i++) {
                selected -= cellTypes[i].levelRates[iLevel];
                if(selected <= 0) {
                    return (CellType)i;
                }
            }

            Debug.Assert(false);
            return CellType.Grass;
        }

        void ShowVisibleCells() {
            if(MainCharacter.instance != null) {
                var centerCell = CellManager.Get(MainCharacter.instance.myTransform.position);
                if(centerCell != null) {
                    for(int x = centerCell.x - visibilityRange;
                        x <= centerCell.x + visibilityRange; x++) {
                        for(int y = centerCell.y - visibilityRange;
                            y <= centerCell.y + visibilityRange; y++) {
                            CellManager.Get(x, y)?.Show();
                        }
                    }
                }
            }
        }
    }
}