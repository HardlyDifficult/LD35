﻿using System;
using UnityEngine;

namespace HD {
    public class TrapDoor : MonoBehaviour {
        public void OnTriggerEnter(Collider other) {
            if(enabled) {
                if(other.GetComponent<IDieFromTraps>() != null) {
                    GetComponent<Animator>().Play("Open");
                    enabled = false;
                }
            }
        }
    }
}