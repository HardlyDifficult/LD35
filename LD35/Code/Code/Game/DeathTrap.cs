﻿using System;
using UnityEngine;

namespace HD {
    public class DeathTrap : MonoBehaviour {
        public void OnTriggerEnter(Collider other) {
            if(enabled) {
                if(other.GetComponent<IDieFromTraps>() != null) {
                    if(other.GetComponent<MainCharacter>() != null) {
                        MainCharacter.instance.GameOver();
                    } else {
                        Destroy(other.gameObject);
                    }
                }
            }
        }
    }
}