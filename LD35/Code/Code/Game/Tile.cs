﻿using System;
using System.Collections;
using UnityEngine;

namespace HD {
    public class Tile : MonoBehaviour {
        public GridCell cell;
        const float scaleSpeed = 0.1f;

        #region Const data
        public Material flippedMaterial;
        public MonoBehaviour flippedScript;
        public bool flipScale;
        MeshRenderer mesh;
        Material originalMaterial;
        float originalScale, flippedScale;
        #endregion Const data

        Coroutine routine;
        bool spawnTrap;
        public void Start() {
            originalScale = transform.localScale.y;
            if(flipScale) {
                flippedScale = originalScale == 1 ? 3 : 1;
            } else {
                flippedScale = originalScale;
            }
            if(flippedMaterial != null) {
                mesh = GetComponent<MeshRenderer>();
                originalMaterial = mesh.material;
            }
            Grid.instance.onFlip += Instance_onFlip;
            if(originalScale != flippedScale) {
                transform.localScale = new Vector3(1, 1, 1);
            }
            if(cell.supportsTraps) {
                spawnTrap = UnityEngine.Random.Range(0, 10) == 0;
            }
            routine = StartCoroutine(DoFlip());
        }

        IEnumerator DoFlip() {
            if(flippedScript != null && !Grid.instance.isWorldFlipped) {
                flippedScript.enabled = false;
            }
            if(flippedMaterial != null && !Grid.instance.isWorldFlipped) {
                mesh.material = originalMaterial;
            }

            float direction;
            if(Grid.instance.isWorldFlipped) {
                direction = flippedScale - transform.localScale.y;
            } else {
                direction = originalScale - transform.localScale.y;
            }

            if(flipScale) {
                while(Grid.instance.isWorldFlipped ? transform.localScale.y != flippedScale
                    : transform.localScale.y != originalScale) {
                    transform.localScale = new Vector3(1,
                        direction > 0 ?
                            Math.Min(Grid.instance.isWorldFlipped ? flippedScale : originalScale, transform.localScale.y + direction * scaleSpeed)
                            : Math.Max(Grid.instance.isWorldFlipped ? flippedScale : originalScale, transform.localScale.y + direction * scaleSpeed),
                        1);
                    yield return new WaitForFixedUpdate();
                }
            }

            if(!IsAnyNeighbor(CellType.Water)) {
                if(flippedMaterial != null && Grid.instance.isWorldFlipped) {
                    mesh.material = flippedMaterial;
                }

                if(flippedScript != null && Grid.instance.isWorldFlipped) {
                    flippedScript.enabled = true;
                }
            }

            if(spawnTrap && flipScale) {
                Instantiate(Grid.instance.spikeTrap, transform.position + Vector3.up / 2, Quaternion.identity);
                spawnTrap = false;
            }

            if(cell.type != CellType.Tree && transform.localScale.y == 1 && UnityEngine.Random.Range(0, 6) == 0) {
                yield return new WaitForSeconds(.1f);
                if((CellManager.Get(transform.position - new Vector3(1, 0, 0))?.tile?.transform.localScale.y ?? 0f) == 3
                    && (CellManager.Get(transform.position + new Vector3(1, 0, 0))?.tile?.transform.localScale.y ?? 0) == 3) {
                    Instantiate(Grid.instance.bridge, transform.position + Grid.instance.bridge.transform.position
                        + new Vector3(0, 0, UnityEngine.Random.Range(0, 2) == 0 ? .2f : -.2f), Quaternion.Euler(0, 90, 0));
                } else if((CellManager.Get(transform.position - new Vector3(0, 0, 1))?.tile?.transform.localScale.y ?? 0) == 3
                     && (CellManager.Get(transform.position + new Vector3(0, 0, 1))?.tile?.transform.localScale.y ?? 0) == 3) {
                    Instantiate(Grid.instance.bridge, transform.position + Grid.instance.bridge.transform.position
                        + new Vector3(UnityEngine.Random.Range(0, 2) == 0 ? .2f : -.2f, 0, 0), Quaternion.identity);
                }
            }
        }

        void Instance_onFlip() {
            if(routine != null) {
                StopCoroutine(routine);
            }
            routine = StartCoroutine(DoFlip());
        }

        private bool IsAnyNeighbor(CellType type) {
            return (CellManager.Get(cell.x - 1, cell.y)?.type.Equals(type) ?? false)
                || (CellManager.Get(cell.x + 1, cell.y)?.type.Equals(type) ?? false)
                || (CellManager.Get(cell.x, cell.y - 1)?.type.Equals(type) ?? false)
                || (CellManager.Get(cell.x, cell.y + 1)?.type.Equals(type) ?? false);
        }
    }
}