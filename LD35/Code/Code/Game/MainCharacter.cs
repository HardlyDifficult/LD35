﻿using System;
using UnityEngine;

namespace HD {
    public class MainCharacter : MonoBehaviour, IDieFromTraps {
        public static MainCharacter instance;
        public CharacterController controller;
        public Transform myTransform;
        public float speed = 100;
        Vector3 previousPosition;
        public void Awake() {
            previousPosition = transform.position;
            instance = this;
            myTransform = transform;
            controller = GetComponent<CharacterController>();
        }

        public void GameOver(bool iWin = false) {
            GameObject.Find("Canvas").GetComponent<CanvasScripts>().gameOverScreen.SetActive(true);
            if(iWin) {
                GameObject.Find("WinSound").GetComponent<AudioSource>().Play();
            } else {
                GameObject.Find("DeathSound").GetComponent<AudioSource>().Play();
            }
            Destroy(gameObject);
        }

        public void OnDisable() {
            instance = null;
        }

        public void Update() {
            if(controller.enabled) {
                if(transform.position != previousPosition) {
                    var facing = (transform.position - previousPosition).normalized;
                    //facing = new Vector3(facing.x, 0, facing.z);

                    var targetRotation = Quaternion.LookRotation(facing).eulerAngles.y + 90; //Quaternion.Euler(0, Vector3.Angle(previousPosition, transform.position), 0);
                    transform.rotation = Quaternion.Euler(0,
                        targetRotation
                        //transform.rotation.eulerAngles.y.Lerp(targetRotation, Time.deltaTime)
                        , 0);
                    previousPosition = transform.position;
                }

                Vector3 target;
                if(myTransform.position.y < CellManager.offset.y - 10) {
                    CellManager.GoDownLevel();
                    transform.position = CellManager.midPointForCurrentLevel + new Vector3(0, 10, 0);
                    target = Vector3.zero;
                } else {
                    var hor = Input.GetAxis("Horizontal");
                    var ver = Input.GetAxis("Vertical");

                    if(hor != 0 || ver != 0) {
                        target = new Vector3(hor + ver, 0, ver - hor);
                        if(target.magnitude > 1) {
                            target = target.normalized;
                        }
                        if(!CellManager.IsInBounds(target / 5 + transform.position)) {
                            target = Vector3.zero;
                        } else {
                            target *= Time.deltaTime * speed;
                        }
                    } else {
                        target = Vector3.zero;
                    }
                }
                controller.SimpleMove(target);
            }
        }
    }
}