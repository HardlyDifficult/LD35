﻿using System;
using UnityEngine;

namespace HD {
    public class BadGuy : MonoBehaviour, IDieFromTraps {
        public float speed;
        CharacterController controller;
        Vector3 direction;
        public void Awake() {
            controller = GetComponent<CharacterController>();
            UpdateDirection();
        }

        public void OnTriggerEnter(Collider other) {
            if(other.GetComponent<MainCharacter>() != null) {
                MainCharacter.instance.GameOver();
            }
        }

        public void Update() {
            try {
                if(MainCharacter.instance != null) {
                    var deltaToCharacter = MainCharacter.instance.transform.position - transform.position;
                    if(deltaToCharacter.magnitude < 2) {
                        var moveDelta = deltaToCharacter.normalized * speed;
                        controller.SimpleMove(moveDelta * Time.deltaTime);
                        return;
                    }
                }

                var delta = direction * Time.deltaTime;
                var target = transform.position + delta / 5; // ???
                var targetCell = CellManager.Get(target);
                if(targetCell.type == CellManager.Get(transform.position).type) {
                    if(targetCell.tile != null) {
                        controller.SimpleMove(delta);
                    } else {
                        targetCell.hasBadGuy = true;
                        Destroy(gameObject);
                    }
                } else {
                    controller.SimpleMove(Vector3.zero);
                    UpdateDirection();
                }
            } catch {
                Destroy(gameObject);
            }
        }

        void UpdateDirection() {
            do {
                direction = new Vector3(UnityEngine.Random.Range(-1, 2), 0, UnityEngine.Random.Range(-1, 2));
            } while(direction == Vector3.zero);
            direction = direction.normalized * speed;
        }
    }
}