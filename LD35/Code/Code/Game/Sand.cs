﻿using System;
using System.Collections;
using UnityEngine;

namespace HD {
    public class Sand : MonoBehaviour {
        bool hasBeenTriggered;

        public void OnTriggerEnter(Collider other) {
            if(!hasBeenTriggered) {
                if(other.GetComponent<IDieFromTraps>() != null) {
                    hasBeenTriggered = true;
                    StartCoroutine(ScaleDown());
                }
            }
        }

        IEnumerator ScaleDown() {
            while(transform.localScale.y > 1) {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y - .005f, transform.localScale.z);
                yield return new WaitForFixedUpdate();
            }

            transform.localEulerAngles = new Vector3(transform.localScale.x, 1, transform.localScale.z);
        }
    }
}