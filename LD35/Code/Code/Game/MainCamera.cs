﻿using System;
using UnityEngine;

namespace HD {
    public class MainCamera : MonoBehaviour {
        Vector3 offset, target;

        public void Awake() {
            offset = transform.position;
        }

        public void LateUpdate() {
            if(MainCharacter.instance != null) {
                target = offset + new Vector3(MainCharacter.instance.transform.position.x,
                    (MainCharacter.instance.transform.position.y < 0 || MainCharacter.instance.transform.position.y > 5) ?
                        MainCharacter.instance.transform.position.y
                        : 0,
                    MainCharacter.instance.transform.position.z);
            }
            var delta = Math.Abs(target.y - transform.position.y);
            if(delta > 50) {
                transform.position = target;
            } else {
                transform.position = Vector3.MoveTowards(transform.position, target, .3f + .055f * delta);
            }
        }
    }
}