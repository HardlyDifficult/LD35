﻿using System;
using UnityEngine;

namespace HD {
    public class RandomZRotationOnSpawn : MonoBehaviour {
        public void Awake() {
            var currentRotation = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(currentRotation.x, currentRotation.y, UnityEngine.Random.Range(0, 360));
        }
    }
}