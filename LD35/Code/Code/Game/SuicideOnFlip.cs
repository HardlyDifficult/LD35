﻿using System;
using UnityEngine;

namespace HD {
    public class SuicideOnFlip : MonoBehaviour {
        public void Awake() {
            Grid.instance.onFlip += Instance_onFlip;
        }

        void Instance_onFlip() {
            Destroy(gameObject);
            Grid.instance.onFlip -= Instance_onFlip;
        }
    }
}