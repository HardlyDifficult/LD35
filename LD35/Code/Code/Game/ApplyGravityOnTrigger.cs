﻿using System;
using UnityEngine;

namespace HD {
    public class ApplyGravityOnTrigger : MonoBehaviour {
        CharacterController controller;
        public void Awake() {
            controller = GetComponent<CharacterController>();
        }

        public void OnTriggerEnter(Collider other) {
            if(enabled && other.GetComponent<IDieFromTraps>() != null) {
                controller.enabled = true;
            }
        }

        public void Update() {
            if(controller.enabled) {
                controller.SimpleMove(Vector3.zero);

                if(transform.position.y < -50) {
                    Destroy(gameObject);
                }
            }
        }
    }
}