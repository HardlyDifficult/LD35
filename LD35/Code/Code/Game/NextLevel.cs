﻿using System;
using System.Collections;
using UnityEngine;

namespace HD {
    public class NextLevel : MonoBehaviour {
        public void OnTriggerEnter(Collider other) {
            if(other.GetComponent<MainCharacter>() != null) {
                StartCoroutine(LevelUp());
            }
        }

        IEnumerator LevelUp() {
            if(MainCharacter.instance != null) {
                GameObject.Find("LevelUpSound").GetComponent<AudioSource>().Play();
                MainCharacter.instance.controller.enabled = false;
                GetComponent<Animator>().Play("Up");
                for(int i = 0; i < 110; i++) {
                    if(MainCharacter.instance != null) {
                        MainCharacter.instance.myTransform.position = new Vector3(MainCharacter.instance.myTransform.position.x,
                            MainCharacter.instance.myTransform.position.y + .2f, MainCharacter.instance.myTransform.position.z);
                        yield return new WaitForFixedUpdate();
                    }
                }

                CellManager.LoadNextLevel(MainCharacter.instance.transform.position);

                MainCharacter.instance.controller.enabled = true;
            }
        }
    }
}