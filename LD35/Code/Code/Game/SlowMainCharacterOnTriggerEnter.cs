﻿using System;
using UnityEngine;

namespace HD {
    public class SlowMainCharacterOnTrigger : MonoBehaviour {
        float slowSpeed = 50, fastSpeed = 80;

        public void OnTriggerEnter(Collider other) {
            if(other.GetComponent<MainCharacter>() != null) {
                MainCharacter.instance.speed = slowSpeed;
            }
        }

        public void OnTriggerExit(Collider other) {
            if(other.GetComponent<MainCharacter>() != null) {
                MainCharacter.instance.speed = fastSpeed;
            }
        }
    }
}